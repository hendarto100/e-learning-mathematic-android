package com.survey.android.math_learning;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class AlurActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alur);
        getSupportActionBar().setTitle("Alur Pembelajaran");
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_import_contacts_black_24dp);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //progressBar();
    }


    public void onBackPressed() { //overrideAnimation
        super.onBackPressed();
        overridePendingTransition(R.anim.sladeinleft, R.anim.sladeoutright);
    }
}
