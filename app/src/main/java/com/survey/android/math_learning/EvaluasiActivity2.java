package com.survey.android.math_learning;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class EvaluasiActivity2 extends AppCompatActivity {
int total;
DataUser user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evaluasi2);
        getSupportActionBar().setTitle("Math Workshop");
        total = total + getIntent().getExtras().getInt("score");
        user = SharedHelper.unpackData(getIntent().getExtras().getString("data"));
        Toast.makeText(this, "Jawaban anda sebelumnya "+user.getJawaban1(), Toast.LENGTH_SHORT).show();


    }


    public void jawabanBenar(View view) {
        Intent intent = new Intent(this,EvaluasiActivity3.class);
        total = total + 1;
        user.setJawaban2( view.getTag().toString());
        intent.putExtra("data",SharedHelper.packData(user));
        intent.putExtra("score",total);
        startActivity(intent);
        overridePendingTransition(R.anim.sladeinrigt, R.anim.sladeoutleft); //for In animation
        total= total - 1;
    }

    public void jawabanSalah(View view) {
        Intent intent = new Intent(this,EvaluasiActivity3.class);
        user.setJawaban2( view.getTag().toString());
        intent.putExtra("data",SharedHelper.packData(user));
        intent.putExtra("score",total);
        startActivity(intent);
        overridePendingTransition(R.anim.sladeinrigt, R.anim.sladeoutleft); //for In animation
    }

}
