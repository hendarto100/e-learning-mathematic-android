package com.survey.android.math_learning;

import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerFullScreenListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;

public class Materi3Activity extends AppCompatActivity {
YouTubePlayerView youTubePlayerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_materi3);
        getSupportActionBar().setTitle("INTEGRAL PANGKAT");
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_import_contacts_black_24dp);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        addFullScreenListenerToPlayer();


    }

    public void onBackPressed() { //overrideAnimation
        super.onBackPressed();
        overridePendingTransition(R.anim.sladeinleft, R.anim.sladeoutright);

    }

    private void addFullScreenListenerToPlayer() {
        youTubePlayerView = findViewById(R.id.youtube_player_view3);
        youTubePlayerView.addFullScreenListener(new YouTubePlayerFullScreenListener() {
            @Override
            public void onYouTubePlayerEnterFullScreen() {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                //fullScreenHelper.enterFullScreen();
                getSupportActionBar().hide();
                findViewById(R.id.mt1).setVisibility(View.GONE);
                findViewById(R.id.mt2).setVisibility(View.GONE);
                findViewById(R.id.mt3).setVisibility(View.GONE);
                findViewById(R.id.mt4).setVisibility(View.GONE);
                findViewById(R.id.mt5).setVisibility(View.GONE);
                findViewById(R.id.mt6).setVisibility(View.GONE);
                findViewById(R.id.mt7).setVisibility(View.GONE);
                findViewById(R.id.mt8).setVisibility(View.GONE);
                findViewById(R.id.mt9).setVisibility(View.GONE);
                findViewById(R.id.mt10).setVisibility(View.GONE);





            }

            @Override
            public void onYouTubePlayerExitFullScreen() {

                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                //fullScreenHelper.exitFullScreen();
                getSupportActionBar().show();
                findViewById(R.id.mt1).setVisibility(View.VISIBLE);
                findViewById(R.id.mt2).setVisibility(View.VISIBLE);
                findViewById(R.id.mt3).setVisibility(View.VISIBLE);
                findViewById(R.id.mt4).setVisibility(View.VISIBLE);
                findViewById(R.id.mt5).setVisibility(View.VISIBLE);
                findViewById(R.id.mt6).setVisibility(View.VISIBLE);
                findViewById(R.id.mt7).setVisibility(View.VISIBLE);
                findViewById(R.id.mt8).setVisibility(View.VISIBLE);
                findViewById(R.id.mt9).setVisibility(View.VISIBLE);
                findViewById(R.id.mt10).setVisibility(View.VISIBLE);




            }
        });
    }

}
