package com.survey.android.math_learning;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class EvaluasiActivity8 extends AppCompatActivity {
int total;
DataUser user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evaluasi8);
        getSupportActionBar().setTitle("Math Workshop");
        total = total + getIntent().getExtras().getInt("score");
        user = SharedHelper.unpackData(getIntent().getExtras().getString("data"));
        Toast.makeText(this, "Jawaban anda sebelumnya "+user.getJawaban7(), Toast.LENGTH_SHORT).show();
    }


    public void jawabanBenar(View view) {
        Intent intent = new Intent(this,EvaluasiActivity9.class);
        total = total + 1;
        intent.putExtra("score",total);
        user.setJawaban8( view.getTag().toString());
        intent.putExtra("data",SharedHelper.packData(user));
        startActivity(intent);
        overridePendingTransition(R.anim.sladeinrigt, R.anim.sladeoutleft); //for In animation
        total= total - 1;
    }

    public void jawabanSalah(View view) {
        Intent intent = new Intent(this,EvaluasiActivity9.class);
        intent.putExtra("score",total);
        user.setJawaban8( view.getTag().toString());
        intent.putExtra("data",SharedHelper.packData(user));
        startActivity(intent);
        overridePendingTransition(R.anim.sladeinrigt, R.anim.sladeoutleft); //for In animation
    }

}
