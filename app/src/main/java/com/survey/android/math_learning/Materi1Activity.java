package com.survey.android.math_learning;

import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerFullScreenListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;

public class Materi1Activity extends AppCompatActivity {
    LinearLayout linearLayout;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_materi1);
        getSupportActionBar().setTitle("PENGERTIAN DAN PENERAPAN");
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_import_contacts_black_24dp);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        addFullScreenListenerToPlayer();


    }

    public void onBackPressed() { //overrideAnimation
        super.onBackPressed();
        overridePendingTransition(R.anim.sladeinleft, R.anim.sladeoutright);

    }

    private void addFullScreenListenerToPlayer() {
        YouTubePlayerView youTubePlayerView = findViewById(R.id.youtube_player_view1);
        youTubePlayerView.addFullScreenListener(new YouTubePlayerFullScreenListener() {
            @Override
            public void onYouTubePlayerEnterFullScreen() {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                //fullScreenHelper.enterFullScreen();
                getSupportActionBar().hide();
                findViewById(R.id.linearLayoutMateri1).setVisibility(View.GONE);


            }

            @Override
            public void onYouTubePlayerExitFullScreen() {

                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                //fullScreenHelper.exitFullScreen();
                getSupportActionBar().show();
                findViewById(R.id.linearLayoutMateri1).setVisibility(View.VISIBLE);



            }
        });
    }


}
