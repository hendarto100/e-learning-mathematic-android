package com.survey.android.math_learning;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import co.nedim.maildroidx.MaildroidX;
import co.nedim.maildroidx.MaildroidXType;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getSupportActionBar().setTitle("Math Workshop");
    }

    public void sk_kd(View view) {
        Intent intent = new Intent(this,Sk_kd.class);
        startActivity(intent);
        overridePendingTransition(R.anim.sladeinrigt, R.anim.sladeoutleft); //for In animation

    }

    public void materi(View view) {
        Intent intent = new Intent(this,MateriActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.sladeinrigt, R.anim.sladeoutleft); //for In animation
    }

    public void evaluasi(View view) {
        Intent intent = new Intent(this,SendMailActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.sladeinrigt, R.anim.sladeoutleft); //for In animation
    }

    public void info(View view) {
        Intent intent = new Intent(this,InfoActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.sladeinrigt, R.anim.sladeoutleft); //for In animation
    }

    public void sendMail(View view) {

    }

    public void diskusi(View view) {
        Intent intent = new Intent(this,DiscussionActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.sladeinrigt, R.anim.sladeoutleft); //for In animation
    }
}
