package com.survey.android.math_learning;

public class DataUser {
    String name,nisn,email,jawaban1,jawaban2,jawaban3,jawaban4,jawaban5,jawaban6,jawaban7,jawaban8,jawaban9,jawaban10;
    String score;

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public DataUser() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNisn() {
        return nisn;
    }

    public void setNisn(String nisn) {
        this.nisn = nisn;
    }

    public String getJawaban1() {
        return jawaban1;
    }

    public void setJawaban1(String jawaban1) {
        this.jawaban1 = jawaban1;
    }

    public String getJawaban2() {
        return jawaban2;
    }

    public void setJawaban2(String jawaban2) {
        this.jawaban2 = jawaban2;
    }

    public String getJawaban3() {
        return jawaban3;
    }

    public void setJawaban3(String jawaban3) {
        this.jawaban3 = jawaban3;
    }

    public String getJawaban4() {
        return jawaban4;
    }

    public void setJawaban4(String jawaban4) {
        this.jawaban4 = jawaban4;
    }

    public String getJawaban5() {
        return jawaban5;
    }

    public void setJawaban5(String jawaban5) {
        this.jawaban5 = jawaban5;
    }

    public String getJawaban6() {
        return jawaban6;
    }

    public void setJawaban6(String jawaban6) {
        this.jawaban6 = jawaban6;
    }

    public String getJawaban7() {
        return jawaban7;
    }

    public void setJawaban7(String jawaban7) {
        this.jawaban7 = jawaban7;
    }

    public String getJawaban8() {
        return jawaban8;
    }

    public void setJawaban8(String jawaban8) {
        this.jawaban8 = jawaban8;
    }

    public String getJawaban9() {
        return jawaban9;
    }

    public void setJawaban9(String jawaban9) {
        this.jawaban9 = jawaban9;
    }

    public String getJawaban10() {
        return jawaban10;
    }

    public void setJawaban10(String jawaban10) {
        this.jawaban10 = jawaban10;
    }
}
