package com.survey.android.math_learning;

import android.content.pm.ActivityInfo;
import android.os.CountDownTimer;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.appcompat.app.AppCompatActivity;
import io.github.kexanie.library.MathView;

import android.os.Bundle;
import android.transition.Fade;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerFullScreenListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;

public class Materi2Activity extends AppCompatActivity {
    LinearLayout linearLayout;
    ProgressBar progressBar;
    ConstraintLayout constraintLayout;
    ScrollView mScrollview;
    Transition transition;
    YouTubePlayerView youTubePlayerView;
    TextView m1t1,m1t2,m1t3,m1t4,m1t8,m1t9;
    MathView m1t0,m1t5,m1t6,m1t7,m1t10,m1t11;
    TextView contoh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_materi2);
        getSupportActionBar().setTitle("INTEGRAL TAK TENTU");
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_import_contacts_black_24dp);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //progressBar();

        youTubePlayerView = findViewById(R.id.youtube_player_view2);
        addFullScreenListenerToPlayer();
        m1t0 = findViewById(R.id.formula);
        m1t1 = findViewById(R.id.m1t1);
        m1t2 = findViewById(R.id.m1t2);
        m1t3 = findViewById(R.id.m1t3);
        m1t4 = findViewById(R.id.m1t4);
        m1t5 = findViewById(R.id.m1t5);
        m1t6 = findViewById(R.id.m1t6);
        m1t7 = findViewById(R.id.m1t7);
        m1t8 = findViewById(R.id.m1t8);
        m1t9 = findViewById(R.id.m1t9);
        m1t10 = findViewById(R.id.m1t10);
        m1t11 = findViewById(R.id.m1t11);

        //m1t0.setVisibility(View.GONE);
    }

    public void onBackPressed() { //overrideAnimation
        super.onBackPressed();
        overridePendingTransition(R.anim.sladeinleft, R.anim.sladeoutright);

    }


    private void addFullScreenListenerToPlayer() {
        youTubePlayerView.addFullScreenListener(new YouTubePlayerFullScreenListener() {
            @Override
            public void onYouTubePlayerEnterFullScreen() {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                //fullScreenHelper.enterFullScreen();
                getSupportActionBar().hide();
                m1t0.setVisibility(View.GONE);
                m1t1.setVisibility(View.GONE);
                m1t2.setVisibility(View.GONE);
                m1t3.setVisibility(View.GONE);
                m1t4.setVisibility(View.GONE);
                m1t5.setVisibility(View.GONE);
                m1t6.setVisibility(View.GONE);
                m1t7.setVisibility(View.GONE);
                m1t8.setVisibility(View.GONE);
                m1t9.setVisibility(View.GONE);
                m1t10.setVisibility(View.GONE);
                m1t11.setVisibility(View.GONE);



            }

            @Override
            public void onYouTubePlayerExitFullScreen() {

                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                //fullScreenHelper.exitFullScreen();
                getSupportActionBar().show();
                m1t0.setVisibility(View.VISIBLE);
                m1t1.setVisibility(View.VISIBLE);
                m1t2.setVisibility(View.VISIBLE);
                m1t3.setVisibility(View.VISIBLE);
                m1t4.setVisibility(View.VISIBLE);
                m1t5.setVisibility(View.VISIBLE);
                m1t6.setVisibility(View.VISIBLE);
                m1t7.setVisibility(View.VISIBLE);
                m1t8.setVisibility(View.VISIBLE);
                m1t9.setVisibility(View.VISIBLE);
                m1t10.setVisibility(View.VISIBLE);
                m1t11.setVisibility(View.VISIBLE);


            }
        });
    }
}
