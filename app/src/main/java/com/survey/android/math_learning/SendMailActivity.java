package com.survey.android.math_learning;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

public class SendMailActivity extends AppCompatActivity {
EditText nama,email,nisn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_mail);
        getSupportActionBar().setTitle("Math Workshop");
        nama = findViewById(R.id.dataNama);
        email = findViewById(R.id.dataEmail);
        nisn = findViewById(R.id.dataNisn);

    }

    public void kerjakan(View view) {
        if(nama.getText().toString().equals("") ||email.getText().toString().equals("") || nisn.getText().toString().equals("") )
        {
            Toast.makeText(this, "Masukan data dengan lengkap", Toast.LENGTH_SHORT).show();
        }
        else{

            String stringNama = String.valueOf(nama.getText());
            String stringEmail = String.valueOf(email.getText());
            String stringNisn  = String.valueOf(nisn.getText());

            SharedPreferences sharedData = getSharedPreferences("DATA",MODE_PRIVATE);
            DataUser user = new DataUser();
            user.setName(stringNama);
            user.setEmail(stringEmail);
            user.setNisn(stringNisn);
            //set variables of 'myObject', etc.

            SharedPreferences.Editor prefsEditor = sharedData.edit();
            Gson gson = new Gson();
            String json = gson.toJson(user);
            prefsEditor.putString("user", json);
            prefsEditor.commit();
            changeActivity();

        }

    }

    public void onBackPressed() { //overrideAnimation
        super.onBackPressed();
        overridePendingTransition(R.anim.sladeinleft, R.anim.sladeoutright);
    }

    public void changeActivity(){
        Intent intent = new Intent(this,EvaluasiActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.sladeinrigt, R.anim.sladeoutleft); //for In animation
    }
}
