package com.survey.android.math_learning;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class SubmitActivity extends AppCompatActivity {
int total;
DataUser user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit);
        total = total + getIntent().getExtras().getInt("score");
        user = SharedHelper.unpackData(getIntent().getExtras().getString("data"));
        Toast.makeText(this, "Jawaban anda sebelumnya "+user.getJawaban10(), Toast.LENGTH_SHORT).show();
    }

    public void submitTest(View view) {
        Intent intent = new Intent(this,HasilEvaluasiActivity.class);
        intent.putExtra("data",SharedHelper.packData(user));
        intent.putExtra("score",total);
        startActivity(intent);
        overridePendingTransition(R.anim.sladeinrigt, R.anim.sladeoutleft);
    }



}
