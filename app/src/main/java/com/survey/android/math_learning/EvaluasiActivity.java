package com.survey.android.math_learning;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.service.autofill.UserData;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;

public class EvaluasiActivity extends AppCompatActivity {
    int total = 0;
    SharedPreferences sharedData;
    DataUser user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evaluasi);
        getSupportActionBar().setTitle("Math Workshop");
        //total = total + getIntent().getExtras().getInt("score");

        //getData from sharedData
        Gson gson = new Gson();
        sharedData = getSharedPreferences("DATA",MODE_PRIVATE);
        String json = sharedData.getString("user", "");
        user = SharedHelper.unpackData(json); //unpack Data from String to Object
        Toast.makeText(this, "Selamat mengerjakan "+user.getName(), Toast.LENGTH_SHORT).show();

    }


    public void jawabanBenar(View view) {
        Intent intent = new Intent(this,EvaluasiActivity2.class);
        user.setJawaban1( view.getTag().toString());
        intent.putExtra("data",SharedHelper.packData(user));
        intent.putExtra("score",1);
        startActivity(intent);
        overridePendingTransition(R.anim.sladeinrigt, R.anim.sladeoutleft); //for In animation
        total= total - 1;
    }

    public void jawabanSalah(View view) {
        Intent intent = new Intent(this,EvaluasiActivity2.class);
        user.setJawaban1( view.getTag().toString());
        intent.putExtra("data",SharedHelper.packData(user));
        intent.putExtra("score",0);
        startActivity(intent);
        overridePendingTransition(R.anim.sladeinrigt, R.anim.sladeoutleft); //for In animation
    }
    

    public void onBackPressed() { //overrideAnimation
        super.onBackPressed();
        overridePendingTransition(R.anim.sladeinleft, R.anim.sladeoutright);
    }



}
