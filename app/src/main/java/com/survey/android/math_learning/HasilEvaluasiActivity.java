package com.survey.android.math_learning;

import androidx.appcompat.app.AppCompatActivity;
import co.nedim.maildroidx.MaildroidX;
import co.nedim.maildroidx.MaildroidXType;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;

public class HasilEvaluasiActivity extends AppCompatActivity {
    TextView mTextView,mJawabanMurid,mJawabanMurid2,mTextLulus;
    DataUser user;
    String emailPack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hasil_evaluasi);
        getSupportActionBar().setTitle("Math Workshop");
        mTextView = findViewById(R.id.hasil);
        mTextView.setText(String.valueOf(getIntent().getExtras().getInt("score")) + "0");
        user = SharedHelper.unpackData(getIntent().getExtras().getString("data"));
        user.setScore(String.valueOf(getIntent().getExtras().getInt("score")) + "0");
        Toast.makeText(this, "nama : " + user.getName() + "jawaban : " + user.getJawaban1() +
                user.getJawaban2() +
                user.getJawaban3() +
                user.getJawaban4() +
                user.getJawaban5() +
                user.getJawaban6() +
                user.getJawaban7() +
                user.getJawaban8() +
                user.getJawaban9() +
                user.getJawaban10(), Toast.LENGTH_SHORT).show();
        mJawabanMurid = findViewById(R.id.jawabanMurid);
        mJawabanMurid2 = findViewById(R.id.jawabanMurid2);
        String SjawabanMurid = " 1." + user.getJawaban1() + "\n"+
                " 2." +
                user.getJawaban2() + "\n"+
                " 3." +
                user.getJawaban3() + "\n"+
                " 4." +
                user.getJawaban4() + "\n"+
                " 5." +
                user.getJawaban5();

        String SjawabanMurid2 = " 6." +
                user.getJawaban6() + "\n"+
                " 7." +
                user.getJawaban7() + "\n"+
                " 8." +
                user.getJawaban8() + "\n"+
                " 9." +
                user.getJawaban9() + "\n"+
                " 10." +
                user.getJawaban10();
        mJawabanMurid.setText(SjawabanMurid);
        mJawabanMurid2.setText(SjawabanMurid2);
        emailPack = SharedHelper.packData(user);
        sendMail();

        mTextLulus = findViewById(R.id.textLulus);
        if(getIntent().getExtras().getInt("score") < 7.5){
            mTextLulus.setText("Anda Belum Lulus Silahkan Mengulang Materi Integral Tak Tentu");
            mTextLulus.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        }

    }

    public void keHome(View view) {

        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("EXIT", true);
        //sendMail();
        startActivity(intent);
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    public void sendMail() {
        new MaildroidX.Builder()
                .smtp("smtp.gmail.com")
                .smtpUsername("workshopmath2020")
                .smtpPassword("fanymonica")
                .port("465")
                .type(MaildroidXType.HTML)
                .to("workshopmath2020@gmail.com")
                .from("Application_MathWorkshop@internet.com")
                .subject("Pekerjaan murid "+user.getName())
                .body(formating())
                .onCompleteCallback(new MaildroidX.onCompleteCallback() {
                    Long timeout = 3000L;
                    @Override
                    public void onSuccess() {
                        Toast.makeText(HasilEvaluasiActivity.this, "Data Terkirim",Toast.LENGTH_SHORT).show();
                        Log.d("email","sended");
                    }

                    @Override
                    public void onFail(@NotNull String s) {
                        Toast.makeText(HasilEvaluasiActivity.this, s,Toast.LENGTH_SHORT).show();
                        Log.d("email",s);

                    }

                    @Override
                    public long getTimeout() {
                        return 0;
                    }
                }).mail();

    }

    String formating(){
        return "Nama :"+user.getName()+"<br>"+
                "NISN :"+user.getNisn()+"<br>"+
                "Email :"+user.getEmail()+"<br>"+
                "Score :"+user.getScore()+"<br>"+
                "1. "+user.getJawaban1()+"<br>"+
                "2. "+user.getJawaban2()+"<br>"+
                "3. "+user.getJawaban3()+"<br>"+
                "4. "+user.getJawaban4()+"<br>"+
                "5. "+user.getJawaban5()+"<br>"+
                "6. "+user.getJawaban6()+"<br>"+
                "7. "+user.getJawaban7()+"<br>"+
                "8. "+user.getJawaban8()+"<br>"+
                "9. "+user.getJawaban9()+"<br>"+
                "10. "+user.getJawaban10()+"<br>";

    }

    public void onBackPressed() {
        Toast.makeText(this, "Anda tidak bisa mengerjakan ulang", Toast.LENGTH_SHORT).show();
    }

    public void keGdrive(View view) {
        String url = "https://drive.google.com/drive/folders/1VBwFj4f0eo01RFWb7pFSHYg8LQGJvm-o?usp=sharing";
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }
}