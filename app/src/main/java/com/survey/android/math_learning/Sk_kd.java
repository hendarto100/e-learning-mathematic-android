package com.survey.android.math_learning;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.github.barteksc.pdfviewer.PDFView;

public class Sk_kd extends AppCompatActivity {

    PDFView pdfView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sk_kd);
        getSupportActionBar().setTitle("KD, IPK dan Tujuan Pembelajaran");
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_import_contacts_black_24dp);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }

    public void onBackPressed() { //overrideAnimation
        super.onBackPressed();
        overridePendingTransition(R.anim.sladeinleft, R.anim.sladeoutright);
    }
}
