package com.survey.android.math_learning;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

import static android.content.Context.MODE_PRIVATE;

public class SharedHelper {
    public static void setDefaults(String key, String value, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }


    public static String getDefaults_JSON(String key, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key, null);
    }
    public static String packData(DataUser C_user){
        Gson gson = new Gson();
        return gson.toJson(C_user); //parsing ke JSON

    }

    public static DataUser unpackData(String C_user){
        Gson gson = new Gson();
        return gson.fromJson(C_user, DataUser.class);
    }

}
