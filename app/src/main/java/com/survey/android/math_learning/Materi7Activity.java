package com.survey.android.math_learning;


import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerFullScreenListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;


public class Materi7Activity extends AppCompatActivity {
FullScreenHelper fullScreenHelper;
YouTubePlayerView youTubePlayerView;
TextView latihanSoal,materi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_materi7);
        getSupportActionBar().setTitle("PENERAPAN INTEGRAL TAK TENTU");
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_import_contacts_black_24dp);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        youTubePlayerView = findViewById(R.id.youtube_player_view7);
        addFullScreenListenerToPlayer();

        latihanSoal = findViewById(R.id.latihanSoalMateri7);
        materi = findViewById(R.id.titleMateri7);




    }

    public void onBackPressed() { //overrideAnimation
        super.onBackPressed();
        overridePendingTransition(R.anim.sladeinleft, R.anim.sladeoutright);

    }

    private void addFullScreenListenerToPlayer() {
        youTubePlayerView.addFullScreenListener(new YouTubePlayerFullScreenListener() {
            @Override
            public void onYouTubePlayerEnterFullScreen() {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                //fullScreenHelper.enterFullScreen();
                getSupportActionBar().hide();
                latihanSoal.setVisibility(View.GONE);
                materi.setVisibility(View.GONE);


            }

            @Override
            public void onYouTubePlayerExitFullScreen() {

                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                //fullScreenHelper.exitFullScreen();
                getSupportActionBar().show();
                latihanSoal.setVisibility(View.GONE);
                materi.setVisibility(View.GONE);


            }
        });
    }



}
