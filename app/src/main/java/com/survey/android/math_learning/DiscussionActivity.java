package com.survey.android.math_learning;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.FileProvider;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FilterQueryProvider;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

public class DiscussionActivity extends AppCompatActivity {

    CardView mCardGoogleMeet;
    Context mContext;
    final int PICK_FROM_GALLERY = 200;
    int columnIndex;
    Uri URI = null;
    String attachmentFile;
    EditText messageEdit;
    FileProvider fileProvider;
    boolean DONE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discussion);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_import_contacts_black_24dp);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mCardGoogleMeet = findViewById(R.id.googleMeet);
        messageEdit = findViewById(R.id.keteranganEmail);
        mCardGoogleMeet.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                ClipboardManager cManager = (ClipboardManager) getApplicationContext().getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData cData = ClipData.newPlainText("text","https://meet.google.com/ukq-byxa-set");
                cManager.setPrimaryClip(cData);
                Toast.makeText(DiscussionActivity.this, "Copied to clipboard", Toast.LENGTH_SHORT).show();
                return true;
            }
        });

    }

    public void onBackPressed() { //overrideAnimation
        super.onBackPressed();
        overridePendingTransition(R.anim.sladeinleft, R.anim.sladeoutright);
    }

    public void videoMeet(View view) {
        String url = "https://meet.google.com/ukq-byxa-set";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }


    public boolean openFolder(){
        Intent intent = new Intent();
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        // Set your required file type
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
        startActivityForResult(Intent.createChooser(intent, "DEMO"),1001);
        return true;
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        try {
            InputStream is = getContentResolver().openInputStream(data.getData());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        URI = FileProvider.getUriForFile(this, "com.survey.android.math_learning.provider", new File(data.getData().getPath()));

        // super.onActivityResult(requestCode, resultCode, data);

       DONE = true;
    }


    public void sendEmail(){
if(1==1) {

    try {
        String email = "workshopmath2020@gmail.com";
        String subject = "FILE DISKUSI";
        String message = "taruh file via attachment";
        Intent emailIntent = new Intent(Intent.ACTION_SEND, Uri.parse("mailto:" + email));
        emailIntent.setType("plain/text");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, message);
        Uri attachments = URI;
 //       emailIntent.putExtra(Intent.EXTRA_STREAM, attachments);
//                if (URI != null) {
//                    emailIntent.putExtra(Intent.EXTRA_STREAM, URI);
//                }
        this.startActivity(Intent.createChooser(emailIntent, "Sending email..."));
    } catch (Throwable t) {
        Toast.makeText(this, "Request failed try again: " + t.toString(), Toast.LENGTH_LONG).show();
    }
}
    }


    public void sendEmailtoGmail(View view) {
        sendEmail();
    }
}

